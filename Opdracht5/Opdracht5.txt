Studentnummer:331516	
Naam: Jasper Rijsbaarman

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# Ja

Waar worden Branches vaak voor gebruikt?
	# Structuur, je kan ervoor zorgen dat bepaalde teams aan een bepaalde branch zitten dan iedereen aan 1 branch

Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2x 

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# $ git branch 
 -Nieuwe Branch aanmaken
	# $ git branch {naam naar behoeve} 
 -Van Branch wisselen
	# $ git checkout {branch}
 -Branch verwijderen
	# $ git branch -d {branch}